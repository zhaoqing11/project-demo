// use es6 module
import { defineConfig } from 'vite'
import legacy from '@vitejs/plugin-legacy'

export default defineConfig({
  plugins: [
    // 设置浏览器兼容性问题
    legacy({
      targets: ['defaults', 'ie 11']
    })
  ]
})
