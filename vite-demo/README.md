Vite

Vite 也是前端构建工具，由于现在浏览器支持原生 esm 模块化，因此 vite 采用 esbuild 预构建工具，esbuild 采用 go 语言编写，因此相对 js 编写的编译器而言速度要快 10-100 倍
相较于 webpack，vite 采用了不同的运行方式：

1. 开发时，不对代码打包，直接采用 EMS 方式运行项目
2. 部署项目时，再对项目进行打包
   除了速度快，vite 使用起来也更加方便
   基本使用：
3. 安装开发依赖 yarn add -D vite
4. vite 的源码目录就是项目根目录
5. 开发命令：
   yarn vite --启动开发服务器
   yarn vite build --打包代码
   yarn vite preview --预览打包后的代码
   使用命令构建
   yarn create vite
   npm create vite@latest
   pnpm create vite
   配置插件 vite.config.js
6. 安装依赖：yarn add @vitejs/plugin-legacy
7. 配置：

// use es6 module
import { defineConfig } from 'vite'
import legacy from '@vitejs/plugin-legacy'

export default defineConfig({
plugins: [
// 设置浏览器兼容性问题
legacy({
targets: ['defaults', 'ie 11']
})
]
})
