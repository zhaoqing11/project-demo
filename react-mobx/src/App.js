import { useStore } from './store/index'
import { observer } from 'mobx-react-lite'

function App() {
  // 重点：解构赋值到store实例对象就可以了 防止破坏响应式 用那个store就解构那个store
  // const rootStore = useStore()
  const { counterStore } = useStore()

  return (
    <div className="App">
      {counterStore.count}
      <button onClick={counterStore.addCount}>+</button>
    </div>
  )
}

// 3.包裹app
export default observer(App)
