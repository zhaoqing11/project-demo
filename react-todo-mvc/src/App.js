import { Button, Input, Space, Table, Tag, Popconfirm } from 'antd'
import axios from 'axios'
import React from 'react'
import './App.css'

const { Search } = Input

class App extends React.Component {
  state = {
    list: [],
    columns: [
      {
        title: '任务编号',
        dataIndex: 'id',
        key: 'id'
      },
      {
        title: '任务名称',
        dataIndex: 'name',
        key: 'name'
      },
      {
        title: '任务描述',
        dataIndex: 'des',
        key: 'des'
      },
      {
        title: '操作',
        render: (_, record) => (
          <Popconfirm
            title="是否确认删除？"
            onConfirm={() => this.delData(_, record)}>
            <a>删除</a>
          </Popconfirm>
        )
      }
    ]
  }

  // 搜索
  onSearch = async value => {
    console.log(value)
    const res = await axios.get(`http://localhost:3001/data/?name=${value}`)
    console.log(res)
    this.setState({
      list: res.data
    })
  }
  // 删除
  delData = async (_, record) => {
    console.log(_, record)
    await axios.delete(`http://localhost:3001/data/${record.id}`)
    this.loadList()
  }
  // 加载列表
  loadList = async () => {
    const res = await axios.get('http://localhost:3001/data')
    console.log(res)
    this.setState({
      list: res.data
    })
  }
  componentDidMount() {
    this.loadList()
  }
  render() {
    return (
      <div className="container">
        {/* <Button type="primary">test</Button> */}

        <Search
          placeholder="请输入关键词..."
          allowClear
          enterButton="搜索"
          size="normal"
          onSearch={this.onSearch}
        />

        <Table columns={this.state.columns} dataSource={this.state.list} />
      </div>
    )
  }
}

export default App
