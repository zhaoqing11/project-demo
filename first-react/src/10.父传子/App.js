import React from 'react'

/**
 * props：
 * 是只读对象，
 * 可以传递任意类型数据（可包含数值、字符串、布尔值、数组、对象、函数、JSX【jsx相当于vue中的插槽】）
 * 不能进行修改
 */
function SonA({ title, list, userInfo, handleClick, jsxTep }) {
  // 解构赋值
  // const { title, list, userInfo, handleClick, jsxTep } = props
  return (
    <div>
      son a,
      {title}
      {list.map((item, index) => (
        <p key={index}>{item}</p>
      ))}
      {userInfo.name}
      <button type="primary" onClick={handleClick}>
        调父组件fn
      </button>
      {jsxTep}
    </div>
  )
}

// 类组件必须通过this关键词获取参数(props是固定的)
class SonB extends React.Component {
  render() {
    return <div>son b,{this.props.title}</div>
  }
}

class App extends React.Component {
  state = {
    title: 'hhhh',
    list: [1, 2, 3],
    userInfo: {
      id: 1,
      name: '李四'
    }
  }
  handleClick = () => {
    console.log('hhhhhhhh')
  }
  render() {
    return (
      <div>
        this is parent
        <SonA
          title={this.state.title}
          list={this.state.list}
          userInfo={this.state.userInfo}
          handleClick={this.handleClick}
          jsxTep={<span>嘿嘿</span>}
        />
        <SonB title={this.state.title} />
      </div>
    )
  }
}

export default App
