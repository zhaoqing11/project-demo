import './app.css'
import React, { createRef } from 'react' /** 非受控组件引入: createRef */

// 受控表单组件（input框自己的状态被React组件状态控制）
class Counter extends React.Component {
  /**
   * 这个实例属性是可以自定义的 语义化即可
   *（非受控组件引入createRef()函数 ）
   */
  msgRef = createRef()
  state = {
    msg: ''
  }
  handleChange = e => {
    // console.log(e)
    this.setState({
      msg: e.target.value
    })
  }
  handleChange2 = () => {
    // 通过msgRef获取input value值
    console.log(this.msgRef.current.value)
  }
  render() {
    return (
      <>
        {/* 受控组件 */}
        <input
          type="text"
          value={this.state.msg}
          onChange={this.handleChange}></input>
        {/* 非受控组件 */}
        <input type="text" ref={this.msgRef}></input>
        <button onClick={this.handleChange2}>获取input值</button>
      </>
    )
  }
}

function App() {
  return (
    <div className="App">
      {/* 渲染函数组件 */}
      <Counter />
    </div>
  )
}

export default App
