import './app.css'
import React from 'react'

// 事件绑定--传递额外参数：
// 1. 一个参数：clickHandle -> ((e) => clickHandle(e))
// 2.两个包含两个以上（形参实参数对应）：clickHandle -> ((e,msg) => clickHandle(e,'xxx'))

// 创建函数组件
function ComponentTest() {
  const clickHandle = (e, msg) => {
    console.log(e, msg)
  }
  return <div onClick={e => clickHandle(e, '噢噢噢噢')}>好困</div>
}

// 创建类组件
class HelloComponent extends React.Component {
  // 事件回调函数（标准写法），避免this指向问题
  // 回调函数中的this指向的是当前组件实例对象
  clickHandle = e => {
    console.log('类组件事件被触发..', e)
  }
  render() {
    return <div onClick={this.clickHandle}>想睡觉</div>
  }
}

function App() {
  return (
    <div className="App">
      {/* 渲染函数组件 */}
      <ComponentTest />
      <HelloComponent />
    </div>
  )
}

export default App
