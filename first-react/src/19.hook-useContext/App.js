import React, { createContext, useState, useContext } from 'react'

const context = createContext()

function ComA() {
  const count = useContext(context)
  return (
    <div>
      ComA
      <br />
      count：{count}
      <ComC />
    </div>
  )
}

function ComC() {
  const count = useContext(context)
  return (
    <div>
      ComC
      <br />
      count：{count}
    </div>
  )
}

function App() {
  const [count, setCount] = useState(10)
  return (
    <context.Provider value={count}>
      <div>
        App,
        <ComA />
        <button onClick={() => setCount(count + 1)}>+</button>
      </div>
    </context.Provider>
  )
}

export default App
