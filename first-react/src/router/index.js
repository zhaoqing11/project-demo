import About from './router/About'
import Home from './router/Home'
import Login from './router/Login'
import Layout from './router/Layout'
import Board from './router/Board'
import NotFound from './router/NotFound'

// 路由配置
// BrowserRouter:
// 作用：包裹整个应用 一个React应用只需要使用一次
// 两种常用模式：1.HashRouter 2.BrowserRouter(推荐)
// HashRouter：使用URL的哈希值实现 http://localhost:3000/#/first
// BrowserRouter: 使用H5的history.pushState API 实现 http://localhost:3000/first

// 传参方式：
// 1.searchParams(推荐)
// 2.params（传参个数较少时使用） 使用这个需要在Route的path中使用:id
import { BrowserRouter, Link, Routes, Route } from 'react-router-dom'

function App() {
  return (
    // 声明当前要用一个非hash模式的路由
    <BrowserRouter>
      {/* 指定跳转的组件 to用来配置路由地址 */}
      {/* <Link to="/">首页 </Link> */}
      <Link to="/about">关于 </Link>
      {/* 路由出口 路由对应的组件会在这里进行渲染 */}
      <Routes>
        {/* 指定路径和组件的对应关系 path代表路径 element代表组件（成对出现） path->element */}
        {/* <Route path="/" element={<Home />}></Route> */}
        <Route path="/" element={<Layout />}>
          {/* 嵌套二级路由 */}
          {/* 默认二级 添加index属性 把它的path去掉 */}
          <Route index element={<Board />}></Route>
        </Route>
        <Route path="/about/:id" element={<About />}></Route>
        <Route path="/login" element={<Login />}></Route>
        {/* 当所有路径都没匹配到时 做兜底显示 未找到 */}
        <Route path="*" element={<NotFound />}></Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
