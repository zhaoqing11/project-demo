
const list = [
  {id:1, name: '张三'},
  {id:2, name: '里斯'},
  {id:3, name: '王五'}
]

function App() {
  return (
    <div className="App">

     <ul>
       { list.map(item=> <li key={item.id}>{item.name}</li> ) }
     </ul>

    </div>
  );
}

export default App;
