import { useState, useEffect } from 'react'

export function useLocalStorage(key, defaultVal) {
  const [msg, setMsg] = useState(defaultVal)
  // 每次只要msg变化 就会自动同步到本地localStorage
  useEffect(() => {
    window.localStorage.setItem(key, msg)
  }, [msg, key])
  return [msg, setMsg]
}
