import { useState, useEffect } from 'react'
import { useLocalStorage } from '../hook/useLocalStorage'

// useEffect作用：为函数组件提供副作用处理

// 副作用：除主作用外的其他作用都是副作用 主作用是根据数据渲染UI 其他都是副作用

// 常见副作用：
// 1.ajax请求
// 2.localstroge处理
// 3.手动修改dom

// useEffect使用规则：
// 1.导入useEffect函数
// 2.在函数中执行 传入回调 并定义副作用
// 3.当通过修改状态更新组件时 副作用会不断执行

// 依赖项控制副作用的执行时机
// 1.默认状态（无依赖项）
// 组件初始化的时候先执行一次 等到每次数据修改组件更新再次执行

// 2.添加一个空数组依赖项
// 组件初始化的时候执行一次

// 3.依赖特定项
// 组件初始化时执行一次 依赖的特定项发生变化会再次执行

// 4.注意事项
// 只要在useeffect回调函数中用到的数据状态 就应该出现再依赖项数组中声明 否则报错
// hook的出现 可以在不使用生命周期概念的情况下也可以书写业务代码

// hook-useEffect 清除副作用
function Test() {
  useEffect(() => {
    let timer = setInterval(() => {
      console.log('...')
    }, 1000)

    // 清除副作用
    return () => {
      clearInterval(timer)
    }
  })
  return <div>this is test</div>
}

function App() {
  const [count, setCount] = useState(0)
  const [name, setName] = useState('hello')
  const [msg, setMsg] = useLocalStorage('tom', 'init val')
  setTimeout(() => {
    setMsg('change')
  }, 500)

  const [flag, setFlag] = useState(true)
  useEffect(() => {
    // 定义副作用
    document.title = count
    document.title = name

    // 此处可以发送网络请求 前提是useEffect需要传入空数组: useEffect(()=>{},[])
    // getApi(...)
  }, [count, name])
  return (
    <div>
      <span>{msg}</span>

      {flag ? <Test /> : null}
      <button onClick={() => setFlag(!flag)}>click</button>

      <button onClick={() => setCount(count + 1)}>{count}</button>
      <button onClick={() => setName('tom')}>{name}</button>
    </div>
  )
}

export default App
