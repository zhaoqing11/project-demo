import React from 'react'
// 里面有各种各样内置的校验规则
import PropTypes from 'prop-types'

// 函数组件props默认值
function Test({ list, pageSize = 10 }) {
  return (
    <div>
      {list.map((item, index) => (
        <p key={index}>{item}</p>
      ))}
      {pageSize}
    </div>
  )
}

// 定义规则
Test.propTypes = {
  list: PropTypes.array.isRequired // 限制类型为数组
}

// 函数组件props默认值 1.只用defaultProps 2.函数参数默认值（推荐的方案）
// 区别：第一种在用的时候组件内部已经有了pageSize这个prop 第二种只有传递的时候组件内部才有这个app
// Test.defaultProps = {
//   pageSize: 10
// }

// 类组件props默认值
class TestClassDefaultProps extends React.Component {
  static defaultProps = {
    pageSize: 10
  }
  render() {
    return <div>{this.props.pageSize}</div>
  }
}

/**
 * children
 * 只要在组件里面写了任意代码，都将在这个组件的props.children属性里面出现
 * 组件的children传值支持：普通文本/普通标签元素/函数/JSX模板
 */

function TestComponentChild({ children }) {
  // children()
  return (
    <span>
      张三,
      {/* {children} */}
      {/* {children.map(item => item)} */}
      {/* 函数组件props默认值 */}
      <Test list={[1, 2, 3]} pageSize={20} />
      {/* 类组件props默认值 */}
      <TestClassDefaultProps pageSize={30} />
    </span>
  )
}

// 子组件
function ListItem({ item, handleDel }) {
  return (
    <>
      你好
      <TestComponentChild>
        <div>this is children</div>
        <p>this is p</p>
        {/* {() => console.log(123)} */}
        {/* {
          <div>
            <p>this is p </p>
          </div>
        } */}
      </TestComponentChild>
      <h3>{item.name}</h3>
      <p>{item.price}</p>
      <p>{item.info}</p>
      <button onClick={() => handleDel(item.id)}>删除</button>
    </>
  )
}

class App extends React.Component {
  state = {
    list: [
      {
        id: 1,
        name: '超级好吃的棒棒糖',
        price: 18.8,
        info: '开业大酬宾，全场8折'
      },
      {
        id: 2,
        name: '超级好吃的大鸡腿',
        price: 34.2,
        info: '开业大酬宾，全场8折'
      },
      {
        id: 3,
        name: '超级无敌的冰激凌',
        price: 14.2,
        info: '开业大酬宾，全场8折'
      }
    ]
  }
  handleDel = id => {
    console.log('id->', id)

    this.setState({
      list: this.state.list.filter(item => item.id !== id)
    })
  }
  render() {
    return (
      <div>
        {this.state.list.map(item => (
          <ListItem key={item.id} item={item} handleDel={this.handleDel} />
        ))}
      </div>
    )
  }
}

export default App
