import React, { createContext } from 'react'

/**
 * 跨组件传递数据
 * App -> A -> C
 * App -> C
 *
 * 注意事项：
 * 1.上层组件和下层组件关系是相对的只要存在就可以使用 通常都会通过app作为数据提供方
 * 2.这里涉及到的语法都是固定的 有两处 提供的位置 value提供数据 获取的位置 {value=> 使用value做什么都可以}
 */

// 1.导入createContext方法并执行，解构提供者Provider和消费者Consumer
const { Provider, Consumer } = createContext()
function SonA() {
  return (
    <div>
      child a
      <SonC />
    </div>
  )
}

function SonC() {
  return (
    <div>
      <p>son c</p>
      {/* 3. 通过Consumer使用数据 */}
      <Consumer>{value => <span>{value}</span>}</Consumer>
    </div>
  )
}

class App extends React.Component {
  state = {
    msg: 'this is msg'
  }
  render() {
    return (
      <div>
        {/* 2. 使用Provider包裹根组件 */}
        <Provider value={this.state.msg}>
          <SonA />
        </Provider>
      </div>
    )
  }
}

export default App
