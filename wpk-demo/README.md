Webpack

构建工具：
当习惯了在 node 中编写代码的方式后，再回到前端编写 html、css、js 这些东西会感觉到各种不便。比如：不能放心的使用模块化规范（浏览器兼容性问题），即时可以使用模块化规范也会面临模块过多时的加载问题。
构建工具即代码“打包工具”，通过构建工具可以将使用 EMS 规范编写的代码在打包时转换成旧的 js 语法
自动补全功能，在文件中引入 js 模块时可以省略后缀不写（.js），webpack 会自动帮忙补全

使用 es 模块化开发，在浏览器端运行时，通过访问 index 页面需要加载多个模块多个请求向服务器发送多次请求，容易出现模块加载问题及浏览器兼容性问题，因此使用构建工具可以解决这一问题，实现将项目多文件打包整合成一个文件，缩小项目体积，提高加载速度，解决浏览器兼容性问题。

webpack
使用步骤
初始化项目 yarn init -y
安装依赖包 webpack（核心代码）、wepack-cli（webpack 的命令行工具）
在项目中新建 src 目录，然后编写代码
然后执行 yarn webpack 对代码进行打包

加载器
webpack 默认只会处理 js 文件，如果希望处理其他类型文件，需要引入 loader
以 css 为例：
使用步骤：
安装：yarn add css-loader -D
配置：css-loader（负责处理 css 文件）、style-loader（负责引入样式让其生效）
module:{
rules:[
{test:/\.css$/i,
use:[‘style-loader’,‘css-loader’]
}]
}
babel代码转换编译器
代码转换编译器，可以将js新语法转换成旧语法，以提高代码兼容性
webpack支持babel，则需要引入babel的loader
使用步骤：
安装 npm install -D babel- loader （连接babel和webpack的中间件）@label/core（核心包） @label/preset-env（预设）
配置：
module:{
rules:[
{test:/\.(m?js)$/,
exclude:/(node_modules)/,
use:{
loader: ‘babel-loader’,
options:{
presets: [‘@babel/preset-env’]
}}}]
}
插件 plugin
用来为 webpack 提供扩展功能
html-webpack-plugin
这个插件可以在代码打包后，自动在打包目录生成 html 页面
使用步骤
安装依赖
配置插件
plugins:[
new HTMLPlugin({
template: ‘./src/index.html’
})
]

开发服务器 webpack-dev-server
安装
yarn add -D webpack-dev-server
启动
yarn webpack serve --open
devtool: ‘inline-source-map’配置源码映射

项目目录说明

package.json
// 存放开发依赖
devDependencies：{}
// 存放项目依赖
dependencies：{}

webpack.config.js
webpack.config.js 是基于 nodejs 的项目，因此要遵循 node 模块化开发规范

src 目录：里面文件是运行在前端，遵循前端 es 开发规范（定义/暴露一个模块：export default {}）
webpack.config.js 是基于 node 服务运行在后端，遵循 node 模块化规范（向外部定义/暴露一个模块：module.exports={}）
