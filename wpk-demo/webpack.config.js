// 引入绝对路径
const path = require('path')
// 引入html插件
const HTMLPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development', // 设置打包模式 production 生产模式 development 开发模式
  entry: './src/index.js', // 用来指定打包时的主文件 默认./src/index.js
  // entry: ['./src/m1.js', './src/m2.js'] // 支持数组
  // entry: {
  //   // 支持对象
  //   a: './src/m1.js',
  //   b: './src/m2.js'
  // },
  output: {
    // 配置代码打包后的地址
    path: path.resolve(__dirname, 'dist'), // 指定打包的目录 必须要绝对路径
    filename: 'main.js', // 打包后的文件包
    // filename: '[name]-[id]-[hash].js', // 针对有多个入口文件打包的情况
    clean: true // 自动清理打包目录
  },
  /**
   * webpack默认只会处理js文件，若需处理其他类型文件则需要引入对应loader
   */
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'] // 有执行顺序 从后往前执行 先转换再引入
      },
      {
        test: /\.(jpg|png|gif)$/i,
        type: 'asset/resource' // 图片资源类型数据，使用type处理
      },
      {
        test: /\.m?js$/, // 匹配以.mjs/js文件进行编译
        exclude: /(node_modules)/, // 剔除node_modules...文件
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'] // 默认设置
          }
        }
      }
    ]
  },
  plugins: [
    new HTMLPlugin({
      // title: 'test',
      template: './src/index.html'
    })
  ],
  devtool: 'inline-source-map' // 将打包后的代码与源代码做映射，方便开发调试
}
